# .vim

My vim dot files. the `.vimrc` file is saved to vimrc.

Just run the following commands via terminal to get perfectly set up:

```
cd ~/
git clone https://gitlab.com/lazypower/dot-vim.git .vim
ln -sf $HOME/.vim/vimrc $HOME/.vimrc
```

# Vundle

I've moved from a pathogen/submodule system into a 
[vundle](https://github.com/VundleVim/Vundle.vim) vendoring system
to keep my dotfiles precisely updated and vendored to mitigate any potential
meta issues with the repository.


